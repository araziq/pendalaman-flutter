# score_count

**Aplikasi penghitung Skor berbasis Flutter.**

## Screenshoot

### Saat pertama kali dibuka

![screenshoot1](docs/img/screen.png)

### Saat tombol Mulai... ditekan dan textfieldform masih dalam kosong

![screenshoot1](docs/img/screen2.png)

### Saat semua nama tim dimasukkan

![screenshoot1](docs/img/screen3.png)

### Saat tombol mulai ditekan aplikasi akan bernavigasi ke halaman score_page

![screenshoot1](docs/img/screen4.png)
![screenshoot1](docs/img/screen5.png)
![screenshoot1](docs/img/screen6.png)

## Demo

![screenshoot1](docs/video/demo.mp4)
