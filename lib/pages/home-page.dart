import 'package:flutter/material.dart';

import 'score-pages.dart';

var _formKey = GlobalKey<FormState>();

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  String team1 = '';
  String team2 = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
          Header(),
          Container(
            padding: EdgeInsets.fromLTRB(16, 32, 16, 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GuideWidget(),
                Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextInput("Tim 1", (value) {
                          team1 = value;
                        }),
                        TextInput("Tim 2", (value) {
                          team2 = value;
                        }),
                        ButtonWidget(() {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ScorePage(
                                        team1: team1,
                                        team2: team2,
                                      )));
                        })
                      ],
                    )),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}

class CurvedClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - 30);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 30);
    path.lineTo(size.width, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: CurvedClipper(),
      child: Container(
        height: MediaQuery.of(context).size.height / 3,
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Colors.blue[300],
              Colors.blue[500],
              Colors.blue[700],
            ],
          ),
        ),
        child: Center(
          child: Text(
            'PENGHITUNG SKOR',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}

class GuideWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      alignment: Alignment.centerLeft,
      child: Text(
        "Masukkan nama-nama tim",
        textAlign: TextAlign.left,
        style: TextStyle(
            fontSize: 12, fontWeight: FontWeight.bold, color: Colors.blue[200]),
      ),
    );
  }
}

class TextInput extends StatelessWidget {
  final String labelText;
  final Function onChangeText;

  const TextInput(this.labelText, this.onChangeText);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
        ),
        onChanged: (value) {
          onChangeText(value);
        },
        validator: (value) {
          if (value.isEmpty) {
            return ('Masukkan nama Tim');
          }
          return null;
        },
      ),
    );
  }
}

class ButtonWidget extends StatelessWidget {
  final Function startBtnHandle;

  ButtonWidget(this.startBtnHandle);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 32),
        child: MaterialButton(
          onPressed: () {
            if (_formKey.currentState.validate()) {
              startBtnHandle();
            }
          },
          child: Text("Mulai...",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          color: Colors.blue,
          colorBrightness: Brightness.dark,
          elevation: 4,
          minWidth: 250,
          padding: EdgeInsets.symmetric(vertical: 16),
          shape: StadiumBorder(),
        ));
  }
}
