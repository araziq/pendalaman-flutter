import 'package:flutter/material.dart';

class ScorePage extends StatefulWidget {
  ScorePage({this.team1, this.team2});

  final String team1;
  final String team2;

  @override
  _ScorePage createState() => _ScorePage();
}

class _ScorePage extends State<ScorePage> {
  int _scoreTeam1 = 0;
  int _scoreTeam2 = 0;

  void _changeScoreTeam1(int val) {
    setState(() {
      _scoreTeam1 += val;
    });
  }

  void _changeScoreTeam2(int val) {
    setState(() {
      _scoreTeam2 += val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Penghitung Skor'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 32),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(16.0),
              child: Row(
                children: [
                  TeamNameWidget(widget.team1),
                  TeamNameWidget(widget.team2),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(16.0),
              child: Row(
                children: [
                  ScoreWidget(_scoreTeam1.toString()),
                  ScoreWidget(_scoreTeam2.toString()),
                ],
              ),
            ),
            Container(
                margin: EdgeInsets.all(16.0),
                child: Row(
                  children: [
                    Expanded(child: ButtonWidgets(_changeScoreTeam1)),
                    Expanded(child: ButtonWidgets(_changeScoreTeam2)),
                  ],
                )),
            Container(
                margin: EdgeInsets.all(16.0),
                child: ButtonWidget(() {
                  Navigator.pop(context);
                })),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.refresh,
        ),
        onPressed: () {
          setState(() {
            _scoreTeam1 = 0;
            _scoreTeam2 = 0;
          });
        },
      ),
    );
  }
}

class TeamNameWidget extends StatelessWidget {
  TeamNameWidget(this.name);
  final String name;
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Text(
      name,
      style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
    ));
  }
}

class ScoreWidget extends StatelessWidget {
  final String score;
  const ScoreWidget(this.score);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Card(
      child: Text(
        score,
        style: TextStyle(fontSize: 64.0, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ));
  }
}

class ButtonWidgets extends StatefulWidget {
  final Function changeScore;
  const ButtonWidgets(this.changeScore);

  @override
  _ButtonWidgets createState() => _ButtonWidgets();
}

class _ButtonWidgets extends State<ButtonWidgets> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        RaisedButton(
          padding: EdgeInsets.all(16),
          shape: CircleBorder(),
          onPressed: () {
            widget.changeScore(1);
          },
          child: Icon(Icons.add),
          color: Colors.green,
          colorBrightness: Brightness.dark,
        ),
        RaisedButton(
          padding: EdgeInsets.all(16),
          shape: CircleBorder(),
          onPressed: () {
            widget.changeScore(-1);
          },
          child: Icon(Icons.remove),
          color: Colors.red,
          colorBrightness: Brightness.dark,
        )
      ]),
    );
  }
}

class ButtonWidget extends StatelessWidget {
  final Function btnHandle;

  ButtonWidget(this.btnHandle);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: MaterialButton(
      onPressed: () {
        btnHandle();
      },
      child: Text("Kembali...",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
      color: Colors.blue,
      colorBrightness: Brightness.dark,
      elevation: 4,
      minWidth: 250,
      padding: EdgeInsets.symmetric(vertical: 16),
      shape: StadiumBorder(),
    ));
  }
}
